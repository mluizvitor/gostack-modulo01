const express = require("express");

const server = express();

server.use(express.json());

// exemplo de 'query params' -> ?parametro=algo
// exemplo de 'route params' -> /rota/:parametro
// exemplo de 'request body' -> { "nome": 'Vitor', "idade": "22"}

const users = ['Mateus', 'Vitor', 'Agata', 'Lenna', 'Becker'];

// middleware global de log
server.use((req, res, next) => {
  console.time('Request');
  console.log(`Método: ${req.method}; URL: ${req.url}`);
  
  next();

  console.timeEnd('Request')
});

// middleware local para checar se o campo 'nome' é enviado no corpo da requisição
function checkNomeField(req, res, next) {
  if(!req.body.nome) {
    return res.status(400).json({error: "Nome de usuário é requerido"})
  }

  return next();
}

// middleware local para checar se o index enviado corresponde a um usuário
function checkIndexExists(req, res, next) {
  const user = users[req.params.index]
  if(!user) {
    return res.status(400).json({error: "Usuário inexistente"})
  }

  req.usuario = user;

  return next();
}


// Lista todos os usuarios
server.get('/users', (req, res) => {
  return res.json(users);
});

// Lista apenas um usuário
server.get('/users/:index', checkIndexExists, (req, res) => {
  return res.json(req.usuario)
});

// Cria novos usuários
server.post('/users', checkNomeField, (req, res) => {
  const { nome } = req.body;
  users.push(nome);

  return res.json(users);
});

// Edita usuários existentes
server.put('/users/:index', checkNomeField, checkIndexExists, (req, res) => {
  const {index} = req.params;
  const {nome} = req.body;

  users[index] = nome;

  return res.json(users);
});

// Exclui usuários existentes
server.delete('/users/:index', checkIndexExists, (req, res) => {
  const {index} = req.params;

  users.splice(index, 1);

  return res.send();
})


server.listen(3000)