# GoStack - Módulo 01

![Language](https://img.shields.io/badge/DEV-Luiz%20Vitor%20Monteiro-E10F36?style=for-the-badge&labelColor=212121) ![Dev](https://img.shields.io/badge/LANG-JavaScript-FEC63F?style=for-the-badge&labelColor=212121)

Este repositório contem o código do `Módulo 01` do curso GoStack da Rocketseat.
Nesse módulo foram feitos rotas de CRUD e middlewares. O conteúdo não tem uma aplicabilidade
específica, apenas serve para estudo.

Foram criadas cinco rotas:

- `GET /users` Para listar todos os usuários existentes.
- `GET /users/:index` Para listar um usuário específico.
- `POST /users` Para criar novos usuários.
- `PUT /users/:index` Para editar um usuário específico.
- `DELETE /users/:index` Para excluir um usuário específico.

E três middlewares:

- `checkNomeField` Para verificar se o campo `nome` foi preenchido.
- `checkIndexExists` Para verificar se o index inserido nos `route params` pode ser acessado.
- Para fazer log de todas as requisições para o console.

## Testando

Para usá-lo é necessário o uso da aplicação `Insomnia` configurada para acessar as rotas.
O [arquivo de configuração](insomnia.json) pode ser usado para agilizar o processo de criação de requests para teste.

Instruções: Abra o `Insomnia` → Clique na seta ao lado do nome do **Workspace** → Vá a `Import/Export` → Toque em `Import Data` → depois em `From File` → selecione o arquivo `insomnia.json` → Após a importação está pronto para testar.

## Sobre

Este repositório está sob a [licença MIT](LICENSE).

Feito com 😃 por Luiz Vitor Monteiro
